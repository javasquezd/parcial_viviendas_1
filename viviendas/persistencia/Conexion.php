<?php
class Conexion{
    /* clase que hace la conexion */
    private $mysqli;
    private $resultado;
    /* ODBC permite conectar a la base de datos */
    function abrir(){
        $this -> mysqli = new mysqli("localhost", "root", "", "viviendas");    /* conectar la bd */    
        $this -> mysqli -> set_charset("utf8"); /* soporta la ñ */
    }

    function cerrar(){
        $this -> mysqli -> close(); /* cierra la conexion */
    }
    
    function ejecutar($sentencia){
        $this -> resultado = $this -> mysqli -> query($sentencia); /* ejecuta dicha sentencia  */
    }
    
    function extraer(){
        return $this -> resultado -> fetch_row();   /* avanza en los resultados de la sentencia sql, debe ser un select */
    }
    
    function numFilas(){
        return ($this -> resultado!=null)?$this -> resultado -> num_rows:0;  /* retornar la cantidad de filas de resultados */
    }    
}
?>