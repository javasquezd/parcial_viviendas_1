<?php
class ProductoDAO{
    private $id;
    private $direccion;
    private $telefono;
    private $barrio;
       
    public function ProductoDAO($id= "", $direccion = "", $telefono = "", $barrio = ""){
        $this -> id = $id;
        $this -> direccion = $direccion;
        $this -> telefono = $telefono;
        $this -> barrio = $barrio;
    }
       
    public function insertar(){
        return "insert into producto (direccion, telefono, barrio)
                values ('" . $this -> direccion . "', '" . $this -> telefono . "', '" . $this -> barrio . "')";
    }
    
    public function consultarTodos(){
        return "select id, direccion, telefono, barrio
                from producto";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select id, direccion, telefono, barrio
                from producto;
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(id)
                from producto";
    }
    
}

?>