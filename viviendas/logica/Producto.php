<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ProductoDAO.php";
class Producto{
    private $id;
    private $direccion;
    private $telefono;
    private $barrio;
    private $conexion;
    private $productoDAO;
    private $cantidad;
    
    public function getId(){
        return $this -> id;
    }
    
    public function getDireccion(){
        return $this -> direccion;
    }
    
    public function getTelefono(){
        return $this -> telefono;
    }
    
    public function getBarrio(){
        return $this -> barrio;
    }

    public function getCantidad(){
        return $this -> cantidad;
    }
        
    public function Producto($id = "", $direccion = "", $telefono = "", $barrio = ""){
        $this -> id = $id;
        $this -> direccion = $direccion;
        $this -> telefono = $telefono;
        $this -> barrio = $barrio;
        $this -> conexion = new Conexion();
        $this -> productoDAO = new ProductoDAO($this -> id, $this -> direccion, $this -> telefono, $this -> barrio);
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> productoDAO -> insertar());        
        $this -> conexion -> cerrar();        
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarTodos());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();        
        return $productos;
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarPaginacion($cantidad, $pagina));
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
}

?>