<?php
$direccion = "";
if(isset($_POST["direccion"])){
    $direccion = $_POST["direccion"];
}
$telefono = "";
if(isset($_POST["telefono"])){
	$telefono = $_POST["telefono"];
}
$barrio = "";
if(isset($_POST["barrio"])){
    $barrio = $_POST["barrio"];
}    
if(isset($_POST["crear"])){
    $producto = new Producto("", $direccion, $telefono, $barrio);
    $producto -> insertar();    
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Crear Producto</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>" method="post">
						<div class="form-group">
							<label>Direccion</label> 
							<input type="text" name="direccion" class="form-control" value="<?php echo $direccion ?>" required>
						</div>
						<div class="form-group">
							<label>Telefono</label> 
							<input type="number" name="telefono" class="form-control" min="1" value="<?php echo $telefono?>" required>
						</div>
						<div class="form-group">
							<label>Barrio</label> 
							<input type="text" name="barrio" class="form-control"  value="<?php echo $barrio ?>" required>
						</div>
						<button type="submit" name="crear" class="btn btn-info">Crear</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>